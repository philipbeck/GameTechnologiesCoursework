#pragma once

#include <ncltech\Scene.h>
#include <ncltech\NetworkBase.h>

#include <ncltech\ListenerManager.h>

//Basic Network Example

class Net1_Client : public Scene
{
public:
	Net1_Client(const std::string& friendly_name);

	virtual void OnInitializeScene() override;
	virtual void OnCleanupScene() override;
	virtual void OnUpdateScene(float dt) override;

	void ProcessNetworkEvent(const ENetEvent& evnt);

protected:
	void SendMazeParams(int size, float density);

	GameObject* box;

	NetworkBase network;
	ENetPeer*	serverConnection;

	//this is the maze generator the client will draw
	MazeGenerator maze;

	//listener stuff
	ListenerManager listenerManager;
	MazeListener mazeListener;
};