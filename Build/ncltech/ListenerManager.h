#pragma once
#ifndef LISTENERMANAGER_H
#define LISTENERMANAGER_H

#include "..\Tuts_GameLogic\Listener.h"

class ListenerManager {
public:
	inline vector<Listener*> GetListeners() const { return listeners; }
	inline void AddListener(Listener* l) { listeners.push_back(l); }

	bool TellListeners(string data);
protected:
	vector<Listener*> listeners;
};

#endif