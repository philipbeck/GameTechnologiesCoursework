#include "PhysicsNode.h"
#include "PhysicsEngine.h"

void PhysicsNode::IntegrateForVelocity(float dt)
{
	/* TUTORIAL 2 CODE */
	//LINEAR
	//first apply gravity
	if (invMass > 0) {
		linVelocity += PhysicsEngine::Instance()->GetGravity() * dt;
	}
	//then work out the rest of the acceleration
	linVelocity += (force*invMass)*dt;
	//apply damping
	linVelocity = linVelocity * PhysicsEngine::Instance()->GetDampingFactor();
	//ANGULAR
	angVelocity += invInertia * torque * dt;
	//apply damping
	angVelocity = angVelocity * PhysicsEngine::Instance()->GetDampingFactor();
}

/* Between these two functions the physics engine will solve for velocity
   based on collisions/constraints etc. So we need to integrate velocity, solve 
   constraints, then use final velocity to update position. 
*/

void PhysicsNode::IntegrateForPosition(float dt)
{
	/* TUTORIAL 2 CODE */
	//linear
	position += linVelocity * dt;
	//angular
	orientation = orientation + Quaternion(angVelocity * dt * 0.5f, 0.0f) * orientation;
	orientation.Normalise();
	//Finally: Notify any listener's that this PhysicsNode has a new world transform.
	// - This is used by GameObject to set the worldTransform of any RenderNode's. 
	//   Please don't delete this!!!!!
	FireOnUpdateCallback();
}