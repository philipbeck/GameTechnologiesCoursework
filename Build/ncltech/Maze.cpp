#include "Maze.h"

#include <utility>

Maze::Maze(int width, int length):
width(width),
length(length){
	maze = new int*[width];
	int** otherMaze = new int*[width];
	for (int i = 0; i < width; i++) {
		maze[i] = new int[length];
		otherMaze[i] = new int[length];
	}
	//first fill the maze with random numbers between 1 and 0
	for (int i = 0; i < width; i++) {
		for (int j = 0; j < length; j++) {
			maze[i][j] = rand()%2;
		}
	}

	//make it into a maze!
	for (int i = 0; i < MAZE_ITERATIONS; i++) {
		for (int j = 0; j < width; j++) {
			for (int k = 0; k < length; k++) {
				otherMaze[j][k] = NextTile(j, k);
			}
		}

		std::swap(maze, otherMaze);
	}

	for (int i = 0; i < width; i++) {
		delete[] otherMaze[i];
	}
	delete[] otherMaze;
}

Maze::~Maze() {
	for (int i = 0; i < width; i++) {
		delete[] maze[i];
	}
	delete[] maze;
}

int Maze::NextTile(int x, int y) {
	//count the number of tiles around (x,y)
	int count = 0;
	for (int i = x - 1; i < x + 2; i++) {
		for (int j = y - 1; j < y + 2; j++) {
			if (i >= 0 && i < width && j >= 0 && j < length &&
				!(i == x && j == y)) {
				count += maze[i][j];
			}
		}
	}
	if (maze[x][y] == 0) {
		if (count == 3) {
			return 1;
		}
	}
	//if there is already a wall here
	else if(count > 0 && count < 6){
		return 1;
	}
	return 0;
}