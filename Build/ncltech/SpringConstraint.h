#pragma once
#ifndef SPRINGCONSTRAINT_H
#define SPRINGCONSTRAING_H

#include "Constraint.h"

class SpringConstraint: public Constraint {
public:
	SpringConstraint(PhysicsNode* obj1, PhysicsNode* obj2,
		const Vector3& globalOnA, const Vector3& globalOnB);
	virtual ~SpringConstraint() {};

	virtual void ApplyImpulse();

	//Draw the constraint visually to the screen for debugging
	virtual void DebugDraw() const;
protected:
	PhysicsNode *pnodeA, *pnodeB;

	float   targetLength;

	Vector3 relPosA;
	Vector3 relPosB;
};

#endif