#include "ListenerManager.h"

bool ListenerManager::TellListeners(string data) {
	DataType tag = Serializable::GetTag(data);
	for (int i = 0; i < listeners.size(); i++) {
		if (listeners[i]->MatchTag(tag)) {
			listeners[i]->Function(data);
			if (!listeners[i]->GetRepeating()) {
				listeners.erase(listeners.begin() + i);
			}
			return true;
		}
	}
	return false;
}