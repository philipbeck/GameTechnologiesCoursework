#include "Octree.h"

//constructors
Octree::Octree(const std::vector<PhysicsNode*>& shapes, Vector3 position, float size, Octree* parent) :
	shapes(std::vector<PhysicsNode*>(shapes)),
	parent(parent),
	position(position),
	size(size),
	children(nullptr)
{
	if (parent) {
		//the octree's level is one greater than it's parent's level
		level = parent->level + 1;
	}
	else {
		//if it hasn't got a level then it is a root node
		level = 0;
	}
}

//destructor
Octree::~Octree(void){
	if (children) {
		for (int i = 0; i < 8; i++) {
			delete children[i];
		}
		delete[] children;
	}
}


//splitting the tree
void Octree::SplitTree() {
	if (level == MAX_LEVELS || shapes.size() <= MAX_SHAPES) {
		return;
	}
	//this means there are too many shapes in this cube and the max level hasn't been reached
	//so the tree must be split again
	//just incase someone calls this function more than once
	if (children) {
		for (int i = 0; i < 8; i++) {
			delete children[i];
		}
		delete[] children;
	}
	//make 8 octrees
	children = new Octree*[8];
	//get half the size
	float halfSize = size / 2;
	for (int x = 0; x < 2; x++) {
		for (int y = 0; y < 2; y++) {
			for (int z = 0; z < 2; z++) {
				//the array position for the shape
				//doesn't really matter which goes where but the order is important
				int i = x + y * 2 + z * 4;
				Vector3 pos = position + (Vector3(x, -y, z) * halfSize);
				children[i] = new Octree(std::vector<PhysicsNode*>(), pos, halfSize, this);
			}
		}
	}
	//check each shape and if it is in more than one
	bool multiple;//if the shape is in more than one child it stays in the parent
	std::vector<PhysicsNode*> newShapes;
	for (std::vector<PhysicsNode*>::iterator shape = shapes.begin(); shape != shapes.end(); shape++) {
		multiple = false;
		Octree* destination = nullptr;
		for (int i = 0; i < 8; i++) {
			if (children[i]->Contains(*shape)) {
				if (destination) {
					//if the shape is already within another node then it stays in the parent
					multiple = true;
					break;
				}
				else {
					destination = children[i];
				}
			}
		}
		if (!multiple) {
			if (destination) {
				destination->shapes.push_back(*shape);
			}
		}
		else {
			newShapes.push_back(*shape);
		}
		//have to check at the end since you don't know if the shape has been removed until afterwards
		//and this wants to be run at least once per vector position
	}
	shapes = newShapes;
	//finally call the split function on all of the children
	for (int i = 0; i < 8; i++) {
		children[i]->SplitTree();
	}
}

bool Octree::Contains(PhysicsNode* shape) {
	Vector3 pos = shape->GetPosition();
	float radius = shape->GetCollisionShape()->GetRadius();

	if (pos.x - radius <= position.x + size &&
		pos.y - radius <= position.y &&
		pos.z - radius <= position.z + size &&
		pos.x + radius >= position.x &&
		pos.y + radius >= position.y - size &&
		pos.z + radius >= position.z) {
		return true;
	}

	return false;
}

void Octree::AddShape(PhysicsNode* shape, bool checked) {
	if (!checked) {
		if (!Contains(shape)) {
			return;
		}
	}
	//put the shape in an array
	shapes.push_back(shape);
}

CollisionStructure Octree::GetPairs() {
	CollisionStructure r;
	//compare this node's shapes to it's other shapes
	if (shapes.size() > 1) {
		for (int i = 0; i < shapes.size() - 1; i++) {
			for (int j = i + 1; j < shapes.size(); j++) {
				GetCollision(r.pairs, shapes[i], shapes[j]);
			}
		}
	}
	//if the node has children get thier shapes and pairs and add them
	if (children) {
		for (int i = 0; i < 8; i++) {
			CollisionStructure c = children[i]->GetPairs();
			//put all the physics nodes in the structure
			r.nodes.insert(r.nodes.begin(), c.nodes.begin(), c.nodes.end());
			r.pairs.insert(r.pairs.begin(), c.pairs.begin(), c.pairs.end());
		}
		//then check to see if any of this node's shapes are colliding with it's children's
		for (int i = 0; i < shapes.size(); i++) {
			for (int j = 0; j < r.nodes.size(); j++) {
				GetCollision(r.pairs, shapes[i], r.nodes[j]);
			}
		}
	}
	//put this nodes shapes into the vector
	r.nodes.insert(r.nodes.begin(), shapes.begin(), shapes.end());
	return r;
}

void Octree::GetPairs(CollisionStructure& r) {
	//compare this node's shapes to it's other shapes
	if (shapes.size() > 1) {
		for (int i = 0; i < shapes.size() - 1; i++) {
			for (int j = i + 1; j < shapes.size(); j++) {
				GetCollision(r.pairs, shapes[i], shapes[j]);
			}
		}
	}
	r.nodes.insert(r.nodes.begin(), shapes.begin(), shapes.end());
	//how many nodes there are so far
	int size = r.nodes.size();
	if (children) {
		for (int i = 0; i < 8; i++) {
			children[i]->GetPairs(r);
		}
		for (int i = 0; i < shapes.size(); i++) {
			for (int j = 0; j < r.nodes.size() - size; j++) {
				GetCollision(r.pairs, shapes[i], r.nodes[j]);
			}
		}
	}
}


//debug draw
void Octree::DebugDraw() {
	NCLDebug::DrawHairLine(position, position + Vector3(size, 0, 0), Vector4(1,0,0,1));
	NCLDebug::DrawHairLine(position, position + Vector3(0, -size, 0), Vector4(1, 0, 0, 1));
	NCLDebug::DrawHairLine(position, position + Vector3(0, 0, size), Vector4(1, 0, 0, 1));

	NCLDebug::DrawHairLine(position + Vector3(size, 0, 0), position + Vector3(size, -size, 0), Vector4(1, 0, 0, 1));
	NCLDebug::DrawHairLine(position + Vector3(size, 0, 0), position + Vector3(size, 0, size), Vector4(1, 0, 0, 1));
	NCLDebug::DrawHairLine(position + Vector3(0, -size, 0), position + Vector3(0, -size, size), Vector4(1, 0, 0, 1));
	NCLDebug::DrawHairLine(position + Vector3(0, -size, 0), position + Vector3(size, -size, 0), Vector4(1, 0, 0, 1));
	NCLDebug::DrawHairLine(position + Vector3(0, 0, size), position + Vector3(0, -size, size), Vector4(1, 0, 0, 1));
	NCLDebug::DrawHairLine(position + Vector3(0, 0, size), position + Vector3(size, 0, size), Vector4(1, 0, 0, 1));

	NCLDebug::DrawHairLine(position + Vector3(0, -size, size), position + Vector3(size, -size, size), Vector4(1, 0, 0, 1));
	NCLDebug::DrawHairLine(position + Vector3(size, -size, 0), position + Vector3(size, -size, size), Vector4(1, 0, 0, 1));
	NCLDebug::DrawHairLine(position + Vector3(size, 0, size), position + Vector3(size, -size, size), Vector4(1, 0, 0, 1));
	if (children) {
		for (int i = 0; i < 8; i++) {
			children[i]->DebugDraw();
		}
	}
}

//test to see if a collision pair is valid and add it to the vector
void GetCollision(vector<CollisionPair>& pairs, PhysicsNode* p1, PhysicsNode* p2) {
	//Check they both at least have collision shapes
	if (p1->GetCollisionShape() != NULL
		&& p2->GetCollisionShape() != NULL)
	{
		if ((p1->GetPosition() - p2->GetPosition()).Length() <
			p1->GetCollisionShape()->GetRadius() + p2->GetCollisionShape()->GetRadius())
		{
			CollisionPair cp;
			cp.pObjectA = p1;
			cp.pObjectB = p2;
			pairs.push_back(cp);
		}
	}
}

