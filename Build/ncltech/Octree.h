#pragma once
#ifndef OCTREE_H
#define OCTREE_H

#include <vector>

#include "PhysicsEngine.h"

#include "PhysicsNode.h"
#include "BoundingBox.h"

#include "Hull.h"

#define MAX_LEVELS 5	//how many levels the tree will go down before it stops
#define MAX_SHAPES 4	//how many shapes can be in a cube for it not to split

struct CollisionStructure {
	std::vector<CollisionPair> pairs;
	std::vector<PhysicsNode*> nodes;
};

class Octree {
public:
	Octree(const std::vector<PhysicsNode*>& shapes = std::vector<PhysicsNode*>(),Vector3 position = Vector3(0,0,0), float size = 32, Octree* parent = nullptr);
	virtual ~Octree(void);
	//return the shapes to be compared with each other
	inline std::vector<PhysicsNode*> GetShapes() const { return shapes; }
	//for making the tree
	void SplitTree();
	//checks if a physics node is in a shape
	bool Contains(PhysicsNode* shape);
	//adds a node to the shape, if checked is false it will test to see if the shape
	//is actually in the node
	void AddShape(PhysicsNode* shape, bool checked = false);
	//recursive function to get all of the collision pairs for the narrow phase
	CollisionStructure GetPairs();
	void GetPairs(CollisionStructure& r);

	//to represent the tree
	void DebugDraw();
protected:
	Octree* parent;
	Octree** children;
	//this is where all of this particular node's shapes will be stored
	std::vector<PhysicsNode*> shapes;
	//these are for checking what's in this square
	int level;
	Vector3 position;
	float size;//how large each side of the cube is
};

//test to see if a collision pair is valid and add it to the vector
void GetCollision(vector<CollisionPair>& pairs, PhysicsNode* p1, PhysicsNode* p2);

#endif