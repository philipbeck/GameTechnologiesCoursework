#pragma once
#ifndef MAZE_H
#define MAZE_H

#include <cmath>

#define MAZE_ITERATIONS 10

class Maze {
public:
	//generates maze
	Maze(int width, int height);
	virtual ~Maze(void);

	int NextTile(int x, int y);

	inline int** GetMaze() { return maze; }
	inline int GetLength() { return length; }
	inline int GetWidth() { return width; }
private:
	int** maze;
	int length;
	int width;
};

#endif