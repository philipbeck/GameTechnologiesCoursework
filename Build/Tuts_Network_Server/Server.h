#pragma once
#ifndef SERVER_H
#define SERVER_H

#include <enet\enet.h>
#include <nclgl\GameTimer.h>
#include <nclgl\Vector3.h>
#include <nclgl\common.h>
#include <ncltech\NetworkBase.h>

#include <Tuts_GameLogic\MazeGenerator.h>

#include <ncltech\ListenerManager.h>

//Needed to get computer adapter IPv4 addresses via windows
#include <iphlpapi.h>
#pragma comment(lib, "IPHLPAPI.lib")

#define SERVER_PORT 1234
#define UPDATE_TIMESTEP (1.0f / 30.0f) //send 30 position updates per second



class Server {
public:
	Server();
	virtual ~Server(void);

	//loops the server until it ends
	void RunServer();

	void Win32_PrintAllAdapterIPAddresses();

	inline bool GetInit() const { return init; }

	friend class MazeParamsListener;
protected:
	void SendMaze();

	NetworkBase server;
	GameTimer timer;
	float accum_time = 0.0f;
	float rotation = 0.0f;

	MazeGenerator mazeGenerator;

	ListenerManager listenerManager;

	bool init;
};

#endif