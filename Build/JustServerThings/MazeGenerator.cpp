#pragma once
#ifndef MAZEGENERATOR_H
#define MAZEGENERATOR_H

#include <ncltech\GameObject.h>
#include <ncltech\Scene.h>
#include "SearchAlgorithm.h"

class MazeGenerator
{
public:
	MazeGenerator(); //Maze_density goes from 1 (single path to exit) to 0 (no walls at all)
	virtual ~MazeGenerator();

	void Generate(int size, float maze_density);

	//All points on the maze grid are connected in some shape or form
	// so any two nodes picked randomly /will/ have a path between them
	GraphNode* GetStartNode() const { return start; }
	GraphNode* GetGoalNode()  const { return end; }
	uint GetSize() const { return size; }


	//Used as a hack for the MazeRenderer to generate the walls more effeciently
	GraphNode* GetAllNodesArr() { return allNodes; }

	//put this in public Serializable can call it
	void Initiate_Arrays();
protected:
	void GetRandomStartEndNodes();

	void Generate_Prims();
	void Generate_Sparse(float density);



public:
	uint size;
	GraphNode *start, *end;

	GraphNode* allNodes;
	GraphEdge* allEdges;
};

#endif