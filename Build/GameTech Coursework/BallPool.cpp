#include "BallPool.h"

#include <nclgl\Vector4.h>
#include <ncltech\GraphicsPipeline.h>
#include <ncltech\PhysicsEngine.h>
#include <ncltech\DistanceConstraint.h>
#include <ncltech\SceneManager.h>
#include <ncltech\CommonUtils.h>

#include <ncltech\CuboidCollisionShape.h>



void BallPool::OnInitializeScene()
{
	Scene::OnInitializeScene();

	//place a ball between max and min
	auto create_ball = [&](Vector3 max, Vector3 min) {
		Vector3 pos;
		pos.x = min.x + (max.x - min.x) * RandFloat();
		pos.y = min.y + (max.y - min.y) * RandFloat();
		pos.z = min.z + (max.x - min.z) * RandFloat();
		//add the sphere
		GameObject* ball = CommonUtils::BuildSphereObject(
			"ball",					// Optional: Name
			pos,				// Position
			1.0 + RandFloat()*1.0,					// Half-Dimensions
			true,				// Physics Enabled?
			1.0f,				// Physical Mass (must have physics enabled)
			true,				// Physically Collidable (has collision shape)
			true,				// Dragable by user?
			Vector4(RandFloat(), RandFloat(), RandFloat(), 1.0));
		ball->Physics()->SetElasticity(0.8);
		AddGameObject(ball);
	};

	//Who doesn't love finding some common ground?
	this->AddGameObject(CommonUtils::BuildCuboidObject(
		"Ground",
		Vector3(0.0f, -1.5f, 0.0f),
		Vector3(20.0f, 1.0f, 20.0f),
		true,
		0.0f,
		true,
		false,
		Vector4(0.2f, 0.5f, 1.0f, 1.0f)));
	//walls!
	this->AddGameObject(CommonUtils::BuildCuboidObject(
		"Wall",
		Vector3(21, 5, 0),
		Vector3(1.0, 6.0, 20.0f),
		true,
		0.0f,
		true,
		false,
		Vector4(0.2f, 0.5f, 1.0f, 1.0f)));
	this->AddGameObject(CommonUtils::BuildCuboidObject(
		"Wall",
		Vector3(-21, 5, 0),
		Vector3(1.0, 6.0, 20.0f),
		true,
		0.0f,
		true,
		false,
		Vector4(0.2f, 0.5f, 1.0f, 1.0f)));
	this->AddGameObject(CommonUtils::BuildCuboidObject(
		"Wall",
		Vector3(0, 5, 21),
		Vector3(20.0, 6.0, 1.0f),
		true,
		0.0f,
		true,
		false,
		Vector4(0.2f, 0.5f, 1.0f, 1.0f)));
	this->AddGameObject(CommonUtils::BuildCuboidObject(
		"Wall",
		Vector3(0, 5, -21),
		Vector3(20.0, 6.0, 1.0f),
		true,
		0.0f,
		true,
		false,
		Vector4(0.2f, 0.5f, 1.0f, 1.0f)));
	//add 200 balls!
	Vector3 maximum(20, 15, 20);
	Vector3 minimum(-20, 0, -20);
	for (int i = 0; i < BALL_NUMBER; i++) {
		create_ball(maximum, minimum);
	}
}

float RandFloat() {
	return (float)rand() / (float)RAND_MAX;
}