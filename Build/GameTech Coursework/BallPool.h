#pragma once

#include <ncltech\Scene.h>
#include <ncltech\CommonUtils.h>

#define BALL_NUMBER 400

//Fully striped back scene to use as a template for new scenes.
class BallPool : public Scene
{
public:
	BallPool(const std::string& friendly_name)
		: Scene(friendly_name)
	{
	}

	virtual ~BallPool(){}

	virtual void OnInitializeScene() override;

};

float RandFloat();