#pragma once
#ifndef LISTENER_H
#define LISTENER_H

#include "..\Tuts_Network_Server\Server.h"

#include "Serializable.h"

class Listener {
public:
	Listener(DataType tag = BAD, bool repeating = false): tag(tag), repeating(repeating){}
	inline bool MatchTag(DataType Tag) { return this->tag == tag; }
	//getters
	inline bool GetRepeating() const { return repeating; }
	//all listeners will have this called
	virtual void Function(string data) = 0;
protected:
	DataType tag;
	//if this is true then the listener will not be removed once it is triggered
	bool repeating;
};

class MazeListener: public Listener {
public:
	MazeListener():Listener() {};
	MazeListener(MazeGenerator&, bool repeating = true);

	void Function(string data) override;
protected:
	//this is the maze generator to be changed
	MazeGenerator* maze;
};

class MazeParamsListener: public Listener {
public:
	MazeParamsListener() :Listener() {};
	MazeParamsListener(Server*, bool repeating = true);

	void Function(string data) override;
protected:
	//this is so this listener can access the server's stuff
	Server* server;
};
#endif