#pragma once
#ifndef SERIALIZABLE_H
#define SERIALIZABLE_H

#include "MazeGenerator.h"

#include <string>

//this is so the program knows what it has been sent
enum DataType{
	MAZE,
	MAZE_PARAMS,
	VECTOR,
	BAD//A tag that will never be used for anything!
};

struct MazeParams {
	int size;
	float density;
};

namespace Serializable {
	//chops the front of a serialized thing and returns the tag
	DataType GetTag(string&);
	//Maze Generator
	std::string SerializeMaze(const MazeGenerator&);
	MazeGenerator GetMaze(std::string);

	std::string SerializeMazeParams(int size, float density);
	MazeParams GetMazeParams(std::string);

	std::string SerializeVector(Vector3);
	Vector3 GetVector(std::string);
}

#endif