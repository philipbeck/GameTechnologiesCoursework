#include "Listener.h"

//MAZE LISTENER
MazeListener::MazeListener(MazeGenerator& maze, bool repeating):
Listener(DataType::MAZE, repeating),
maze(&maze)
{}

//this assumes that the data is correctly laid out
void MazeListener::Function(string data) {
	*maze = Serializable::GetMaze(data);
}


//++++++++++++++MAZE PARAMS LISTENER+++++++++++++++++++
MazeParamsListener::MazeParamsListener(Server* server, bool repeating) :
	Listener(DataType::MAZE_PARAMS, repeating),
	server(server){}

void MazeParamsListener::Function(string data) {
	MazeParams p = Serializable::GetMazeParams(data);
	server->mazeGenerator.Generate(p.size, p.density);
}