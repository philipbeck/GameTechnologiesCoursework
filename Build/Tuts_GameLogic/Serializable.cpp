#include "Serializable.h"

DataType Serializable::GetTag(string& data) {
	DataType r;
	int pos = data.find(":");
	//cast the int into a 
	r = (DataType) stoi(data.substr(0, pos));
	//remove the tag and colon
	data.erase(0, pos + 1);
	//return the tag
	return r;
}


std::string Serializable::SerializeMaze(const MazeGenerator& maze) {
	std::string data;
	//tag
	data += to_string(DataType::MAZE) + ":";
	//size
	int size = maze.GetSize();
	//the only important data for the nodes is the size
	data += std::to_string(size);
	//this will be a list of the edges that exist
	data += ":";
	bool first = true;
	for (int i = 0; i < size * (size - 1); i++) {
		if (maze.allEdges[i]._iswall) {
			if (!first) {
				data += ",";
			}
			else {
				first = false;
			}
			data += to_string(i);
		}
	}
	//start
	data += ":" + std::to_string(maze.start->_pos.x) + ","
		+ std::to_string(maze.start->_pos.y) + ","
		+ std::to_string(maze.start->_pos.z);
	//end
	data += ":" + std::to_string(maze.end->_pos.x) + ","
		+ std::to_string(maze.end->_pos.y) + ","
		+ std::to_string(maze.end->_pos.z);
	return data;
}

MazeGenerator Serializable::GetMaze(std::string data) {
	std::string next;
	MazeGenerator maze;
	int pos = data.find(":");
	next = data.substr(0,pos);
	maze.size = std::stoi(next);
	//fill the maze up
	maze.Initiate_Arrays();
	data.erase(0, pos + 1);
	//get all of the walls
	pos = data.find(":");
	next = data.substr(0, pos);
	data.erase(0, pos + 1);
	bool finished = false;
	while (!finished) {
		pos = next.find(",");
		//if pos is zero that means that the match is either at position 0 or hasn't been found
		//luckily the comma will never be at zero
		if (pos == 0) {
			finished = true;
			maze.allEdges[stoi(next)]._connected = true;
		}
		else {
			maze.allEdges[stoi(next.substr(0,pos))]._connected = true;
			next.erase(0, pos + 1);
		}
	}
	pos = data.find(":");
	next = data.substr(0, pos);
	data.erase(0, pos + 1);
	Vector3 v = GetVector(next);
	maze.start->_pos = v;
	//get end position
	v = GetVector(data);
	maze.end->_pos = v;

	return maze;
}

//maze params
std::string Serializable::SerializeMazeParams(int size, float density) {
	string data = "";
	//tag
	data += to_string(DataType::MAZE_PARAMS) + ":";
	data += to_string(size);
	data += ":";
	data += to_string(density);

	return data;
}

MazeParams Serializable::GetMazeParams(string data) {
	MazeParams r;

	int pos = data.find(":");
	r.size = stoi(data.substr(0, pos));
	data.erase(0, pos + 1);
	r.density = stof(data);

	return r;
}

//vector 3
string Serializable::SerializeVector(Vector3 v) {
	string data;
	//tag
	data += to_string(DataType::VECTOR) + ":";

	data = to_string(v.x)
		+ "," + to_string(v.y)
		+ "," + to_string(v.z);
	return data;
}

Vector3 Serializable::GetVector(string data) {
	Vector3 v;
	int pos = data.find(",");
	v.x = stoi(data.substr(0, pos));
	data.erase(0, pos + 1);
	pos = data.find(",");
	v.y = stoi(data.substr(0, pos));
	data.erase(0, pos + 1);
	v.z = stoi(data);
	return v;
}